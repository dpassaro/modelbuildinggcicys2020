"""
gCICY1 - A modification of the pyCICY package to compute line bundle
cohomologies on the gCICY defined by the following configuration
matrix:

np.array([[1,1,1],[1,1,1],[1,1,1],[1,0,2],[1,3,-1]])

Authors
-------
Davide Passaro (davide.passaro@physics.uva.nl)
Robin Schneider (robin.schneider@physics.uu.se)
"""

# libraries
import numpy as np
import scipy as sc
import sympy as sp
from pyCICY import CICY
import logging
from random import randint
import random
import time
import os
import itertools as it

logging.basicConfig(format='%(name)s:%(levelname)s:%(message)s')
logger = logging.getLogger('gCICY')


class gCICY2(CICY):

    def __init__(self, M, log=3):

        self.M = np.array(M)

        self.ndenom = 3
        self.line_break = 0
        for i in range(len(M[0])):
            if -1 in np.sign(M.T[i]):
                self.line_break = i
                break
        self.M1 = self.M[:, 1:i].T
        self.Ms = CICY(self.M[:, [0,1]])
        self.M2 = self.M[:, i::].T
        logger.setLevel(level=int(log*10))
        super(gCICY2, self).__init__(M, log=log)
        self.fav = True


    def _fill_moduli(self, seed):
        r"""Determines a tuple with monomials and their (redundant) complex moduli
             as coefficient for the defining Normal sections.
        
        Args:
            seed (int): Random seed for the coefficients
        
        Returns:
            tuple 
                1: (nested list: int): nested list of all monomials
                2: (nested list: int): nested list of all coefficients
        """
        random.seed(seed)
        # declare return lists
        dim = int(self._brackets_dim(self.M1[0]))
        M1monomial = self._makepoly(self.M1[0], dim)
        # fill with random values, generic polynomials
        # could make it optional to give the range of the values
        M1moduli = np.array([randint(1, 10) for j in range(dim)], dtype=np.int16)

        #fill M2 monomials and moduli
        tmp_monomials_s = np.copy(M1monomial[0:8])
        tmp_monomials_rest = np.copy(M1monomial)
        # set the x degress to zero
        tmp_monomials_s[:,-1] = 0
        tmp_monomials_rest[:, [-1,-2]] = 0

        # find the right coefficients
        tmp_coeff_s = np.copy(M1moduli[0:8])
        tmp_coeff_t = np.copy(M1moduli)
        tmp_coeff_u = np.copy(M1moduli)
        # multiply the alternating d_0i with minus signs for u
        tmp_coeff_u[8:16] = tmp_coeff_u[8:16]*-1
        tmp_coeff_u[24:] = tmp_coeff_u[24:]*-1

        # (D: multiply again by a random integer) for each section
        # there is 3 from x_3,0/1 degree 2 polynomial and 
        # three from the three denominators
        tmp_coeff_s = np.concatenate([tmp_coeff_s*randint(1,3),tmp_coeff_s*randint(1,3),tmp_coeff_s*randint(1,3)])
        tmp_coeff_t = np.concatenate([tmp_coeff_t*randint(1,3),tmp_coeff_t*randint(1,3),tmp_coeff_t*randint(1,3)])
        tmp_coeff_u = np.concatenate([tmp_coeff_u*randint(1,3),tmp_coeff_u*randint(1,3),tmp_coeff_u*randint(1,3)])

        # fill s monomials
        tmp_monomials_s[:,-2] = -1
        monomials_sA = np.copy(tmp_monomials_s)
        monomials_sA[:,-4] = 2
        monomials_sB = np.copy(tmp_monomials_s)
        monomials_sB[:,-4] = 1
        monomials_sB[:,-3] = 1
        monomials_sC = np.copy(tmp_monomials_s)
        monomials_sC[:,-3] = 2
        monomials_s = np.concatenate([monomials_sA, monomials_sB, monomials_sC])

        # fill t/u monomials
        monomials_restA = np.copy(tmp_monomials_rest)
        monomials_restA[:,-4] = 2
        monomials_restA[:,-4] = 2
        monomials_restB = np.copy(tmp_monomials_rest)
        monomials_restB[:,-4] = 1
        monomials_restB[:,-4] = 1
        monomials_restB[:,-3] = 1
        monomials_restB[:,-3] = 1
        monomials_restC = np.copy(tmp_monomials_rest)
        monomials_restC[:,-3] = 2
        monomials_restC[:,-3] = 2
        monomials_rest = np.concatenate([monomials_restA, monomials_restB, monomials_restC])

        unique_monomials = np.unique(monomials_rest, axis = 0)
        unique_coeff_t = np.zeros(len(unique_monomials)).astype(np.int)
        unique_coeff_u = np.zeros(len(unique_monomials)).astype(np.int)

        for i, m in enumerate(unique_monomials):
            same = np.where(np.all(monomials_rest == m, axis = -1))[0]
            #print(same, m)
            unique_coeff_t[i] = np.sum(tmp_coeff_t[same])
            unique_coeff_u[i] = np.sum(tmp_coeff_u[same])
            
        monomials_rest = np.swapaxes([unique_monomials, unique_monomials], 0,1)
        monomials_rest[:,0,-2] = -1
        monomials_rest[:,1,-1] = -1

        moduli = [M1moduli]+[[tmp_coeff_s, tmp_coeff_t, tmp_coeff_u]]
        pmoduli = [M1monomial]+[[monomials_s.reshape((24,1,10)), np.copy(monomials_rest), np.copy(monomials_rest)]]
        return pmoduli, moduli

    def line_co(self, L, space=True):
        r"""
        The main function of this CICY toolkit. 
        It determines the cohomology of a line bundle over the CY.
        Uses Bott-Borel-Weil theorem and splitting of Koszul sequence. 
        By default makes use of the index and vanishing theorem
        to shorten computation time.
        
        Parameters
        ----------
        L : array[nProj]
            The line bundle L.
        
        Returns
        -------
        hodge: array[nfold+1]
            hodge numbers of the line bundle L.

        See also
        --------
        line_co_euler: Returns the index of a line bundle

        Example
        -------
        >>> M = gCICY2(np.array([[1,1,1],[1,1,1],[1,1,1],[1,0,2],[1,3,-1]]))
        >>> M.line_co([2,1,-3,2,1])
        [0, 72, 0, 0]

        References
        ----------
        .. [1] CY - The Bestiary, T. Hubsch
            http://inspirehep.net/record/338506?ln=en

        .. [2] Heterotic and M-theory Compactifications for String Phenomenology, L. Anderson
            https://arxiv.org/abs/0808.3621

        """

        start = time.time()
        # start with simple Kodaira check to avoid unnecessay computations
        if np.array_equal(L, np.zeros(len(L))):
            return [1, 0, 0, 1]
        elif np.all(np.array(L) > 0):
            return [self.line_co_euler(L), 0, 0, 0]
        elif np.all(np.array(L) < 0):
            return [0, 0, 0, -1*self.line_co_euler(L)]

        # we proceed stepwise: First compute v2, v1 on M and then
        # the maps between H(M,v2) ---rational--> H(M,v1)
        tab2, _ = self.Ms.Leray(self.Ms._line_to_BBW(np.array(L-self.M2[0])))
        v2 = [[],[]]
        v2j = [-1,-1]
        # only k = 0,1 we want to map from k1 -> k0
        for i, wedge_k in enumerate(tab2):
            for j, entry in enumerate(wedge_k):
                # only one j value can have non zero entry
                if entry != 0:
                    v2[i] = entry[0]
                    v2j[i] = j
                    logger.info('V2:Found non trivial entry {} for L = {} at j={} and k={}.'.format(v2[i], L-self.M2[0], j, i))
                    break
        tab1, _ = self.Ms.Leray(self.Ms._line_to_BBW(L))
        v1 = [[],[]]
        v1j = [-1,-1]
        # only k = 0,1 we want to map from k1 -> k0
        for i, wedge_k in enumerate(tab1):
            for j, entry in enumerate(wedge_k):
                # only one j value can have non zero entry
                if entry != 0:
                    v1[i] = entry[0]
                    v1j[i] = j
                    logger.info('V1:Found non trivial entry {} for L = {} at j={} and k={}.'.format(v1[i], L, j, i))
                    break

        hodge = [0 for j in range(self.nfold+1)]
        # next determine all the maps on M we have to compute
        h1 = [[] for _ in range(self.nfold+2)]
        m1, m2 = [], [] 
        dimv1 = [self.Ms._brackets_dim(np.abs(v1[0])) if len(v1[0]) > 0 else 0,
                 self.Ms._brackets_dim(np.abs(v1[1])) if len(v1[1]) > 0 else 0]
        dimv2 = [self.Ms._brackets_dim(np.abs(v2[0])) if len(v2[0]) > 0 else 0,
                 self.Ms._brackets_dim(np.abs(v2[1])) if len(v2[1]) > 0 else 0]
        logger.debug('Found non trivial entries on M with dimensions v1: {} and v2: {}'.format(dimv1, dimv2))
        if v1j[0] == v1j[1] and v1j[0] != -1:
            # need to compute this map
            m1 = self.Ms._single_map(v1[1], dimv1[1], v1[0], dimv1[0], 0)
            rank = np.linalg.matrix_rank(m1)
            if rank == np.min(dimv1):
                # maximal
                if rank == dimv1[0] and not rank == dimv1[1]:
                    h1[v1j[1]-1] += [[np.copy(m1), [v1[1]], [()], False, dimv1[1] - rank]]
                elif rank == dimv1[1] and not rank == dimv1[0]:
                    h1[v1j[0]] += [[np.copy(m1.T), [v1[0]], [(0,)], False, dimv1[0] - rank]]
            else:
                h1[v1j[0]] += [[np.copy(m1.T), [v1[0]], [(0,)], False, dimv1[0] - rank]]
                h1[v1j[1]-1] += [[np.copy(m1), [v1[1]], [()], False, dimv1[1] - rank]]
        else:
            if v1j[0] != -1:
                h1[v1j[0]] += [[0, [v1[0]], [()], True, dimv1[0]]]
            if v1j[1] != -1:
                h1[v1j[1]-1] += [[0, [v1[1]], [()], True, dimv1[1]]]

        h2 = [[] for _ in range(self.nfold+2)]
        if v2j[0] == v2j[1] and v2j[0] != -1:
            m2 = self.Ms._single_map(v2[1], dimv2[1], v2[0], dimv2[0], 0)
            # image map
            rank = np.linalg.matrix_rank(m2)
            if rank == np.min(dimv2):
                # maximal
                if rank == dimv2[0] and not rank == dimv2[1]:
                    h2[v2j[1]-1] += [[np.copy(m2), [v2[1]], [()], False, dimv2[1] - rank]]
                elif rank == dimv2[1] and not rank == dimv2[0]:
                    h2[v2j[0]] += [[np.copy(m2.T), [v2[0]], [(0,)], False, dimv2[0] - rank]]
            else:
                h2[v2j[0]] += [[np.copy(m2.T), [v2[0]], [(0,)], False, dimv2[0] - rank]]
                h2[v2j[1]-1] += [[np.copy(m2), [v2[1]], [()], False, dimv2[1] - rank]]
        else:
            if v2j[0] != -1:
                h2[v2j[0]] += [[0, [v2[0]], [(0,1)], True, dimv2[0]]]
            if v2j[1] != -1:
                h2[v2j[1]-1] += [[0, [v2[1]], [(0,1)], True, dimv2[1]]]
        logger.info('H1: {}.'.format([[(hl[-1], hl[1], hl[2]) for hl in hi] if len(hi) > 0 else 0 for hi in h1]))
        logger.info('H2: {}.'.format([[(hl[-1], hl[1], hl[2]) for hl in hi] if len(hi) > 0 else 0 for hi in h2]))
        # Now we check the maps on X
        # we have 0 -> h2 -> h1 -> h -> 0
        for j in range(self.nfold+2):
            if len(h1[j]) != 0:
                if len(h2[j]) != 0:
                    image = 0
                    dim1 = [entry1[-1] for entry1 in h1[j]]
                    dim2 = [entry2[-1] for entry2 in h2[j]]
                    logger.info('Non trivial map from {} to {}'.format([entry1[1][0] for entry1 in h2[j]],
                                                                         [entry1[1][0] for entry1 in h1[j]]))
                    if j == 0 or j == self.nfold+1:
                        # j= 0/4 map has to be maximal in order to preserve ses. 
                        image += np.min([np.sum(dim1), np.sum(dim2)])
                        if j == 0:
                            hodge[0] += np.sum(dim1)-image
                        else:
                            hodge[self.nfold] += np.sum(dim2)-image
                    else:
                        logger.info('With shapes: {}.'.format([dim2, dim1]))
                        hodge[j] += np.sum(dim1)
                        map = np.zeros((np.sum(dim1), np.sum(dim2)))
                        for i, entry1 in enumerate(h1[j]):
                            for k, entry2 in enumerate(h2[j]):
                                # check if this one is actually allowed from the normal bundle
                                # while there can be two entries only one can map via the normal bundle
                                if np.all(np.abs(np.array(entry1[1][0])-np.array(entry2[1][0])) == np.abs(self.M2[0])):
                                    m3 = self._single_gmap(entry2[1][0], self._brackets_dim(np.abs(entry2[1][0])),
                                                            entry1[1][0], self._brackets_dim(np.abs(entry1[1][0])), 1)
                                    logger.debug('gmap with shape: {}'.format(m3.shape))
                                else:
                                    logger.info('Computing higher order: {} -> {}'.format(entry2[1][0], entry1[1][0]))
                                    m3, _ = self._higher_map(entry2, entry1)
                                    m3 = m3.T
                                    logger.debug('higher with shape {}'.format(m3.shape))
                                if not entry1[-2]:
                                    if not entry2[-2]:
                                        m3 = np.matmul(m3, sc.linalg.null_space(entry2[0]))
                                        m3 = np.matmul(sc.linalg.null_space(entry1[0]).T, m3)
                                    else:
                                        m3 = np.matmul(sc.linalg.null_space(entry1[0]).T, m3)
                                else:
                                    if not entry2[-2]:
                                        m3 = np.matmul(m3, sc.linalg.null_space(entry2[0]))
                                logger.debug('shapes: {} and {}'.format([(np.sum(dim1[0:i]),np.sum(dim1[0:i+1])),
                                     (np.sum(dim2[0:k]), np.sum(dim2[0:k+1]))], m3.shape))
                                map[np.round(np.sum(dim1[0:i])).astype(np.int):np.round(np.sum(dim1[0:i+1])).astype(np.int),
                                     np.round(np.sum(dim2[0:k])).astype(np.int):np.round(np.sum(dim2[0:k+1])).astype(np.int)] += m3
                        rank = np.linalg.matrix_rank(map)
                        hodge[j] -= rank
                        hodge[j-1] += np.sum(dim2)-rank
                else:
                    # then we simple map all of h1[j] into h[j]
                    for entry1 in h1[j]:
                        hodge[j] += entry1[-1]
            else:
                # then we simply map all of h2[j] into h[j-1]
                for entry2 in h2[j]:
                    hodge[j-1] += entry2[-1]

        end = time.time()
        if self.doc:
            logger.info('Finally, we find h = {}.'.format(hodge))
            logger.info('The calculation took: {}.'.format(end-start))
        
        return hodge

    def _higher_map(self, space1, space2):
        r"""Generates the higher projection maps between two spaces.

        Parameters
        ----------
        space1 : list
            space1: [matrix, space, origin, simple, dim]
        space2 : list
            space: [matrix, space, origin, simple, dim]

        Returns
        -------
        array(dim(space1), dim(space2))
            Map from space1 to space2
        """
        v1dim = [self._brackets_dim(space1[1][i]) for i in range(len(space1[1]))]
        v2dim = [self._brackets_dim(space2[1][i]) for i in range(len(space2[1]))]
        map = np.zeros((np.sum(v1dim), np.sum(v2dim)))
        nsec1 = len(space1[2][0])
        nsec2 = len(space2[2][0])
        for i, entry1 in enumerate(space1[1]):
            entry1 = np.abs(entry1)
            for j, entry2 in enumerate(space2[1]):
                entry2 = np.abs(entry2)
                missing_maps = list(set(space1[2][i]).difference(space2[2][j]))
                if len(missing_maps) == nsec1-nsec2:
                    # construct intermediate tensors
                    # we maximize wrt to interdemidate tensor
                    missing_maps = np.sort(missing_maps).tolist()
                    if entry1[-1] > entry2[-1]:
                        missing_maps = missing_maps[::-1]
                    logger.debug('Constructing intermediate tensor(s) with maps {}.'.format(missing_maps))
                    inter_tensors = np.zeros((self.len, len(missing_maps)))
                    for r in range(self.len):
                        # there is some ambiguity here, when it comes
                        # to raising and lowering new tensors.
                        for t in it.product([1,-1], repeat=len(missing_maps)):
                            degree = entry1[r]-entry2[r]
                            for s, k in zip(t,missing_maps):
                                degree += s*self.N[r, k]
                            if degree == 0:
                                inter_tensors[r] = np.array(t)
                                break
                    tmp_map = []
                    for k in range(len(missing_maps)):
                        if len(tmp_map) == 0:
                            target = entry1+inter_tensors[:,k]*self.N[:, missing_maps[k]]
                            target = target.astype(np.int)
                            logger.debug('From {} to {}.'.format(entry1, target))
                            if missing_maps[k] == 0:
                                tmp_map = self._single_map(entry1, v1dim[i],  target, self._brackets_dim(target), missing_maps[k])
                            else:
                                tmp_map = self._single_gmap(entry1, v1dim[i],  target, self._brackets_dim(target), missing_maps[k])
                            logger.debug('Intermediate rank: {}'.format(np.linalg.matrix_rank(tmp_map)))
                        else:
                            target_next = target + inter_tensors[:,k] * self.N[:, missing_maps[k]]
                            target_next = target_next.astype(np.int)
                            logger.debug('From {} to {}.'.format(target, target_next))
                            if missing_maps[k] == 0:
                                new_map = self._single_map(target, self._brackets_dim(target), target_next, 
                                                 self._brackets_dim(target_next), missing_maps[k])
                            else:
                                new_map = self._single_gmap(target, self._brackets_dim(target), target_next, 
                                                 self._brackets_dim(target_next), missing_maps[k])
                            #logger.debug(new_map)
                            if new_map.shape[1] == tmp_map.shape[0]:
                                tmp_map = np.matmul(tmp_map.T, new_map.T)
                            else:
                                tmp_map = np.matmul(tmp_map, new_map.T)
                            logger.debug('Intermediate rank: {}'.format(np.linalg.matrix_rank(tmp_map)))
                            target = np.copy(target_next)
                    map[int(np.sum(v1dim[0:i])):int(np.sum(v1dim[0:i]))+v1dim[i], int(np.sum(v2dim[0:j])):int(np.sum(v2dim[0:j]))+v2dim[j]] += tmp_map
        if not space1[3]:
            if not space2[3]:
                final_map = np.matmul(np.matmul(space1[0].T, map), space2[0])
            else:
                final_map = np.matmul(space1[0].T, map)
        else:
            if not space2[3]:
                final_map = np.matmul(map, space2[0])
            else:
                final_map = map
        return final_map, space2[2]

    def _single_gmap(self, V1, dim_V1, V2, dim_V2, t):
        r"""Determine the matrix of shape (dim_V2,dim_V1) for the rational map from V1 to V2.
        
        Args:
            V1 (list: int): Line bundle in bracket notation
            dim_V1 (int): dimension of V1
            V2 (list: in): Line bundle in bracket notation
            dim_V2 (int): dimension of V2
            t (int): specifies the normal section used for the map
        
        Returns:
            (nested list: int): A matrix for the map between the two monomial basis.
                with entries being the corresponding complex modulis.
        """
        V2 = np.abs(V2)
        V1 = np.abs(V1)
        diff = np.sign(V2-V1)
        diff[-1] = -1*diff[-1]
        smatrix = np.zeros((dim_V2, dim_V1), dtype=np.int32)
        v1basis = self._makepoly(V1, dim_V1)
        v2basis = self._makepoly(V2, dim_V2)
        
        rational_maps = [np.copy(m) for m in self.pmoduli[1]]
        # run over all flipped signs in the difference
        for i in range(len(diff)):
            # run over all sections, since they are lists rather than np.arrays
            for j in range(self.ndenom):
                # change the sign of each degree in the map for the corresponding 
                # projective space
                rational_maps[j][:,:,2*i:2*(i+1)] *= diff[i]   

        if self.doc:
            start = time.time()

        for i in range(dim_V1):
            # loop over all sections
            for j in range(self.ndenom):
                # loop over all monomials per section
                for k in range(len(rational_maps[j])):
                    # loop over all summands in denominator
                    for s in range(len(rational_maps[j][k])):
                        monomial = v1basis[i]+rational_maps[j][k][s]
                        # determine the new monomial
                        if self.is_good_monomial(monomial, V2):
                            #np approach; factor of ~150 faster
                            l = np.where(np.all(monomial == v2basis, axis=1))[0][0]
                            if j != 2:
                                smatrix[l][i] += self.moduli[1][j][k]
                            else:
                                smatrix[l][i] -= self.moduli[1][j][k]
                            #pos = self.decoder(monomial, V2, dim_V2)
                            #smatrix[i][pos] = self.moduli[t][j]
        if self.doc:
            end = time.time()
            logger.debug('Time: {}'.format(end-start))

        return smatrix

    def is_good_monomial(self, m, v):
        r"""Checks if the monomial m is in V.

        Parameters
        ----------
        m : list[nVariables]
            monomial
        v : list[nProjective]
            degrees of v

        Returns
        -------
        bool
            True if in V
        """
        tmp = m.reshape(5,2)
        if np.array_equal(np.add.reduce(np.abs(tmp), axis=1), np.abs(v)):
            if np.array_equal(np.sign(np.add.reduce(np.sign(tmp), axis=1)), np.sign(v)):
                return True
        return False



if __name__ == '__main__':
    #conf = np.array([[1,1,1],[1,1,1],[1,1,1],[1,0,2],[1,3,-1]])
    #M = gCICY2(conf)
    print('done')
