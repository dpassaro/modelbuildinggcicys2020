#from pyCICY import CICY
#from cCICY import CICY
from gCICY1 import gCICY1
from gCICY2 import gCICY2
import numpy as np
import itertools
from scan import scan

kmax = 6
rank = 2

if __name__ == '__main__':

    # First gCICY
    M1 = gCICY1(np.array([[1,1,1],[1,1,1],[1,1,1],[1,1,1],[1,3,-1]]))
    scan(M1, 'line', 'gCICY1', kmax, rank, redundancies=np.array([0,1,2,3]))

    # second gCICY
    M2 = gCICY2(np.array([[1,1,1],[1,1,1],[1,1,1],[1,0,2],[1,3,-1]]))
    scan(M2, 'line', 'gCICY2', kmax, rank, redundancies=np.array([0,1,2]))
