# Scans

Scans for CICYs and gCICYs.

For the Sparse rank computation [SpaSM](https://github.com/cbouilla/spasm) is needed. Put the *rank_hybrid* from SpaSM with the *rank* from the C++ code into the directory from where you run the cCICY code.
