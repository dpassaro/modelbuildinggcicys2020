from wolframclient.evaluation import WolframLanguageSession
from wolframclient.language import wl, wlexpr
#import logging
from gCICY1 import gCICY1
from gCICY2 import gCICY2
from pyCICY import CICY
import numpy as np

#logging.basicConfig(level=logging.INFO)
#logger = logging.getLogger('Stability')

def mathematica_check(umodels, M):
    r"""Uses mathematicas NMinimize to numerically check for stability.

    Parameters
    ----------
    umodels : list(V)
        List of models
    M : CICY
        underlying CICY

    Returns
    -------
    tuple(list[bool], list[bool])
        (success, failures)
    """
    session = WolframLanguageSession()#kernel_loglevel=logging.ERROR
    line_slope = []
    for m in umodels:
        tmp_all = []
        for line in m:
            tmp = M.line_slope()
            for i in range(M.len):
                tmp = tmp.subs({'m'+str(i): line[i]})
            tmp_all += [tmp]
        line_slope += [tmp_all]
    str_slope = [str(l).replace('**', '^').replace('[','{').replace(']', '}') for l in line_slope]
    str_cone = str(['t'+str(i)+'> 1' for i in range(M.len)]).replace('\'', '').replace('[','{').replace(']', '}')
    str_vars = str(['t'+str(i) for i in range(M.len)]).replace('\'', '').replace('[','{').replace(']', '}')
    success = []
    failed = []
    print(len(str_slope))
    for slope, i in zip(str_slope, range(len(str_slope))):
        #print(i)
        full_string = 'NMinimize[Join[{{Plus @@ (#^2 & /@ {})}}, '.format(slope)
        full_string += '{}],'.format(str_cone)
        full_string += '{}, AccuracyGoal -> 20, PrecisionGoal -> 20, WorkingPrecision -> 20]'.format(str_vars)
        optimize = wlexpr(full_string)
        results = session.evaluate(optimize)
        if np.allclose([0.], [results[0].__float__()], atol=0.01):
            success += [i]
        else:
            failed += [[i, results[0].__float__()]]
    session.terminate()
    return success, failed

if __name__ == "__main__":

    conf = np.array([[1,1,1],[1,1,1],[1,1,1],[1,1,1],[1,3,-1]])
    M1 = gCICY1(conf)
    
    print('Starting with gCICY1.')

    m1_models = np.load('gCICY1/6/line_GUTmodels.npy')
    print('Loaded {} models.'.format(m1_models.shape))
    success, fails = mathematica_check(m1_models, M1)

    print('Found {} out of {} models to be stable.'.format(len(success), len(m1_models)))
    np.save('gCICY1/6/stable_GUTmodels.npy', m1_models[np.array(success)])

    conf = np.array([[1,1,1],[1,1,1],[1,1,1],[1,0,2],[1,3,-1]])
    M2 = gCICY2(conf)

    m2_models = np.load('gCICY2/6/line_GUTmodels.npy')
    print('Loaded {} models.'.format(m2_models.shape))
    success, fails = mathematica_check(m2_models, M2)

    print('Found {} out of {} models to be stable.'.format(len(success), len(m2_models)))
    np.save('gCICY2/6/stable_GUTmodels.npy', m2_models[np.array(success)])

