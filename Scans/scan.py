"""
Scanning algorithm to find systematically all line bundles sums
leading to realistic heterotic compactifications.

Authors
-------
Davide Passaro (davide.passaro@physics.uva.nl)
Robin Schneider (robin.schneider@physics.uu.se)
"""

from pyCICY import CICY
#import gCICY_1
#import gCICY_2
import numpy as np
import multiprocessing as mp
import itertools
import operator
import timeit
import random
import time
import os
from joblib import Parallel, delayed
import logging

logger = logging.getLogger('Scan')

def check_for_l1(pair, rank, indices, lines0, M):
    r"""Checks for the conditions in 3.

    i) -3*\Gamma <= ind(pair)
    ii) -3*\Gamma <= ind(\wedge pair) <= 0

    also checks for no enhanced gauge symmetries.

    Parameters
    ----------
    pair : tuple(int)
        indices of the pair in lines0
    rank : int
        -3 * |\Gamma|
    indices : list[int]
        list of all indices
    lines0 : list[lines]
        list of all line bundles, L0
    M : CICY
        underlying CICY

    Returns
    -------
    bool
        True if good tuple
    """
    # conditions 3
    if np.sum(indices[pair]) >= rank:#condition 3.i
        tmp = np.add.reduce(lines0[pair])
        if not np.array_equal(tmp, np.zeros(len(tmp))):#no enhanced gauge symmetries
            tmp = M.line_co_euler(tmp)
            if tmp <= 0 and tmp >= rank:#condition 3.ii
                return True
    return False

def check_for_l2(triplet, rank, lines0, indices, k, M):
    r"""Checks for the conditions in 4.

    i) -2*k <= sum(c1(triples)) <= 2*k
    ii) -3*\Gamma <= ind(triplet)
    iii) -3*\Gamma <= ind(l1 * l3) <= 0
    iv) -3*\Gamma <= ind(\wedge triplet) <= 0

    also checks for no enhanced gauge symmetries.

    Parameters
    ----------
    triplet : tuple(int)
        indices of the triplet in lines0
    rank : int
        -3 * |\Gamma|
    lines0 : list[lines]
        list of all line bundles, L0
    indices : list[int]
        list of all indices
    k : int
        kmax
    M : CICY
        underlying CICY

    Returns
    -------
    bool
        True if good tuple
    """
    #conditions 4
    tmp = np.add.reduce(lines0[triplet])
    if np.max(tmp) <= 2*k and np.min(tmp) >= -2*k:#condition 4.i
        if np.add.reduce(indices[triplet]) >= rank:#condition 4.ii
            if not np.array_equal(tmp, np.zeros(len(tmp))):#no enhanced gauge symmetries
                tmp = M.line_co_euler(np.add.reduce(lines0[[triplet[1], triplet[2]]]))#condition 4.iii
                if tmp >= rank and tmp <= 0:
                    tmp = np.round(np.sum([M.line_co_euler(np.add.reduce(lines0[[l,k]])) for l,k in itertools.combinations(list(triplet), r=2)]))
                    if tmp >= rank and tmp <= 0:#condition 4.iv
                        return True
    return False

new_pairs = np.array([[2,3],[0,4],[1,4],[2,4],[3,4]])

def check_model(L14, lines0, indices, rank, k, M):
    r"""Checks for the conditions in 5 and 6. 
    Given a quadruple of line bundles.

    i) c1(L14+L5) = 0
    check conditions on L5
    ii) \mu(L5) = 0, 0 >= \ind(L5) >= -3*|\Gamma|, h2(L5) = 0
    iii) ind(V) = -3*|\Gamma|
    iv) -3*\Gamma = ind(\wedge^2 triplet)
    v) ind(La * Lb) <= 0
    vi) c2(M) >= c2(V) >= 0

    Parameters
    ----------
    L14 : tuple(int)
        indices of the quadruple in lines0
    lines0 : list[lines]
        list of all line bundles, L0
    indices : list[int]
        list of all indices
    rank : int
        -3 * |\Gamma|
    k : int
        kmax
    M : CICY
        underlying CICY

    Returns
    -------
    bool
        True if good tuple
    """
    L5 = -np.add.reduce(lines0[L14])# condition 5.i
    if np.max(np.abs(L5)) <= k and np.max(L5) != 0:
        if M.l_slope(L5)[0]:#Kahler
            tmp = np.round(M.line_co_euler(L5)).astype(np.int)# condition 5.ii
            if tmp >= rank and tmp <= 0:
                tmp += np.sum(indices[L14]).astype(np.int)# condition 6.i
                if tmp == rank:#, but since we have all 5 we can directl check if -3 gamma
                    h = M.line_co(L5) 
                    if h[2] == 0 and h[0] == 0:# no anti generations
                        quint = np.vstack([lines0[L14],L5])
                        tmp = np.round(np.sum([M.line_co_euler(np.add.reduce([quint[l],quint[k]])) for l, k in itertools.combinations(range(5), r=2)])).astype(np.int)
                        if tmp == rank:# condition 6.ii
                            tmp = np.round(np.sum([M.line_co_euler(np.add.reduce([quint[l[0]],quint[l[1]]])) for l in new_pairs])).astype(np.int)
                            if tmp <= 0:# condition 6.iii
                                c2 = -1/2*np.einsum('rst,st -> r', M.triple, np.einsum('is,it->st', quint, quint))
                                if np.all(np.einsum('ijk,jk->i', M.triple, M.c2_tensor) >= c2) and np.all(c2 >= 0):# condition 6.iv
                                    return True
    return False

def make_quadruples(List):
    # makes the quadruple indices.
    o_t = List[0,:2]
    t_f = np.array(list(itertools.combinations_with_replacement(List[:,2],2)))
    return np.hstack([np.add(o_t,np.zeros((len(t_f),2),np.int)),t_f])
vec_make_quadruples = np.vectorize(make_quadruples,signature='(n,3)->(m,4)')

def check_model_representations(model, M):
    r"""Checks if there is at least one Higgs doublet, i.e.
    h^2(\wedge^2 V) > 0

    Parameters
    ----------
    model : np.array[5, M.len]
        V, sum of line bundles
    M : CICY
        underlying CICY

    Returns
    -------
    bool
        True if satisfied.
    """
    tmp = np.array([M.line_co_euler(np.add.reduce([model[l], model[k]])) for l,k in itertools.combinations(range(5), r=2)])
    if np.all(tmp <= 0):
        # higgs doublets
        tmp = np.add.reduce([M.line_co(np.add.reduce([model[l], model[k]])) for l,k in itertools.combinations(range(5), r=2)])
        if np.round(tmp[2]) > 0:
            return True
    return False

def generate_lines0(M, k, fname, rank):
    r"""Generates the list L0

    Parameters
    ----------
    M : CICY
        underlying CICY
    k : int
        kmax
    fname : str
        filename to save list
    rank : int
        -3*|\Gamma|
    """
    start_time = timeit.default_timer()
    allLines = itertools.product(range(-k,k+1), repeat = M.len)
    lines0 = list()
    indices = list()
    for line in allLines:
        line = list(line)
        if M.l_slope(line)[0]:
            tmpindex = np.round(M.line_co_euler(line)).astype(np.int)
            if rank <= tmpindex <= 0 and tmpindex%rank == 0: # If the index is good
                h = M.line_co(line)
                if h[0] == h[2] and h[0] == 0: # and the hodge numbers are
                    lines0.append(list(line)) # add to the list
                    indices.append(tmpindex)
    elapsed = timeit.default_timer()-start_time
    logger.info("Lines0 built in {} seconds".format(elapsed))
    lines0 = np.array(lines0)
    indices = np.array(indices)
    np.save(fname, lines0)
    np.save(fname+'i', indices)

def generate_add_lines0(M, k, kmax, rank):
    r"""Generate additional lines if L0 exists from previous run.

    Parameters
    ----------
    M : CICY
        underlying CICY
    k : int
        kmax
    kmax : int
        maximal k value of previous list
    rank : int
        -3* |\Gamma|

    Returns
    -------
    tuple(lines, indices)
        additional line0 and indices
    """
    start_time = timeit.default_timer()
    allLines = itertools.product(range(-k,k+1), repeat = M.len)
    lines0 = list()
    indices = list()
    for line in allLines:
        line = np.hstack(line)
        if np.max(np.abs(line) > kmax):
            if M.l_slope(line)[0]:
                tmpindex = np.round(M.line_co_euler(line)).astype(np.int)
                if rank <= tmpindex <= 0 and tmpindex%rank == 0: # If the index is good
                    h = M.line_co(line)
                    if h[0] == h[2] and h[0] == 0: # and the hodge numbers are
                        lines0.append(list(line)) # add to the list
                        indices.append(tmpindex)
    elapsed = timeit.default_timer() - start_time
    logger.info("Additional lines0 built in {} seconds".format(elapsed))
    lines0 = np.array(lines0)
    indices = np.array(indices)
    return lines0, indices

def trim_lines0(M, k, rank, lines, indices, fname):
    r"""Takes a given lines 0 and returns all lines with
    charges in the range k-kmax.

    Parameters
    ----------
    M : CICY
        underlying CICY
    k : int
        kmax
    rank : int
        -3*|\Gamma|
    lines : list
        list of line bundles
    indices : list
        list of indices
    fname : str
        filename to save the new list

    Returns
    -------
    tuple(lines, indices)
        trimmed tuple of lines and indices
    """
    kmax = np.max(np.abs(lines))
    
    if k > kmax:
        new_lines, new_indices = generate_add_lines0(M, k, kmax, rank)
        if len(new_lines) > 0:
            new_lines = np.vstack([lines, new_lines])
            new_indices = np.vstack([indices, new_indices])
        else:
            new_lines = lines
            new_indices = indices
        np.save(fname, new_lines)
        np.save(fname+'i', new_indices)
        return new_lines, new_indices

    good_indices = np.ones(len(lines), dtype=np.bool)
    good_indices[np.where(np.any(np.abs(lines) > k, axis = 1))[0]] = False

    return lines[good_indices], indices[good_indices]

def complete_tuple(tuple, lines0):
    r"""Finds the index of L5.

    Parameters
    ----------
    tuple : tuple(int)
        quadruple of integers L1-L4
    lines0 : list
        L0

    Returns
    -------
    int
        index of L5
    """
    last = -np.add.reduce(lines0[tuple])
    i = np.where(np.all(lines0 == last, axis=1))[0]
    if len(i) != 0:
        return i[0]
    else:
        # interesting case, this shouldnt happen or even be possible?
        logger.warning('Found missing line bundle {} sth. went wrong. {}'.format(last, tuple))
        return -1

def find_r_indices(M, lines0, kmin, symmetries):
    r"""Returns a list of lines with symmetries removed.

    Parameters
    ----------
    M : CICY
        underlying CICY
    lines0 : list
        L0
    kmin : int
        minimum charge
    symmetries : list
        list of integers, with equivalent projective spaces

    Returns
    -------
    list(int)
        list of indices in L0
    """
    symmetries = np.array(symmetries)
    good_indices = np.ones(len(lines0), dtype=np.bool)
    for i, line1 in enumerate(lines0):
        tmp_line = np.zeros(M.len)
        if np.max(np.abs(line1)) < kmin:
            good_indices[i] = False
        if good_indices[i]:
            for subline2 in itertools.permutations(line1[symmetries], r=len(symmetries)):
                tmp_line = np.copy(line1)
                tmp_line[symmetries] = np.array(subline2)
                j = np.where(np.all(lines0[i+1:] == tmp_line, axis=1))[0]
                if len(j) > 0:
                    good_indices[j[0]+i+1] = False
    r_indices = np.arange(len(lines0), dtype=np.int)
    r_indices = r_indices[good_indices]
    return r_indices

def scan(M, fname, dirname, k, rank, verbose=10, kknown=-1, redundancies=list(range(5))):
    r"""Main scan function.

    Parameters
    ----------
    M : CICY
        CICY to scan over
    fname : str
        filename for intermediate results
    dirname : str
        dirname to save intermediate results
    k : int
        kmax
    rank : int
        |\Gamma|
    verbose : int, optional
        verbose statements, by default 10
    kknown : int, optional
        kmax of previous scans, by default -1
    redundancies : list(int), optional
        symmetries of the projective spaces, by default list(range(5))

    Returns
    -------
    int
        0 - saves results to file
    """
    rank = -3*rank
    fname_m = os.path.join(dirname, fname)

    # prepare dir
    if not os.path.exists(dirname):
        os.makedirs(dirname)

    # prepare subdir
    dirname = os.path.join(dirname, str(k))
    if not os.path.exists(dirname):
        os.makedirs(dirname)

    logger.setLevel(logging.DEBUG)
    logger.info("Starting scan with k = {} on CICY: \n {}".format(k, M.M))

    # load previously generated lines
    newly_generated = False
    if not os.path.exists(fname_m+'.npy'):
        logger.info('Creating new line0.')
        generate_lines0(M, k, fname_m, rank)
        newly_generated = True
    
    lines0 = np.load(fname_m+'.npy')
    indices = np.load(fname_m+'i.npy')
    if not newly_generated:
        lines0, indices = trim_lines0(M, k, rank, lines0, indices, fname_m)

    fname = os.path.join(dirname, fname)

    # save in the corresponding new subdirectory
    np.save(fname, lines0)
    np.save(fname+'i', indices)
    n_proc = mp.cpu_count()

    #Find lines1
    start_time = timeit.default_timer()
    r_indices = find_r_indices(M, lines0, kknown, redundancies)
    logger.info('Found {} r-indices.'.format(len(r_indices)))
    #lines0r = lines0[r_indices]
    pairs = np.array(list(itertools.product(*[r_indices, range(len(lines0))], repeat=1)))
    logger.info('Pairs shape before: {}.'.format(pairs.shape))
    pairs = np.unique(np.sort(pairs), axis=0)
    logger.info('Pairs shape after: {}.'.format(pairs.shape))
    #find good pairs
    par_res = Parallel(n_jobs=n_proc, verbose=verbose)(delayed(check_for_l1)(p, rank, indices, lines0, M) for p in pairs)
    #Pick out the good pairs
    lines1 = pairs[par_res]
    elapsed = timeit.default_timer()-start_time
    logger.info("Lines1 built in {} seconds".format(elapsed))

    #Find lines2
    #   Order lines1
    start_time = timeit.default_timer()
    lines1ordered = list()
    lines1ordered.append(list())
    for i in range(len(lines1)-1):
        lines1ordered[-1].append(lines1[i])
        if not lines1[i][0] == lines1[i+1][0]:
            lines1ordered.append(list())
    lines1ordered = [np.array(lines) for lines in lines1ordered[:-1]]
    elapsed = timeit.default_timer()-start_time
    logger.info("Lines1 ordered in {} seconds".format(elapsed))

    #   Make triplets
    start_time = timeit.default_timer()
    #logger.info(lines1ordered)
    triplets = np.vstack([np.array([[l[0,0], x[0], x[1]] for x in itertools.combinations_with_replacement(np.unique(l), 2)]) for l in lines1ordered])
    logger.info('Triplets shape before: {}.'.format(triplets.shape))
    triplets = np.unique(np.sort(triplets), axis=0)
    logger.info('Triplets shape after: {}.'.format(triplets.shape))
    elapsed = timeit.default_timer() - start_time
    logger.info("Triplets made in {} seconds.".format(elapsed))

    #   Select good triplets
    start_time = timeit.default_timer()
    #find good pairs
    par_res = Parallel(n_jobs=n_proc, verbose=verbose)(delayed(check_for_l2)(t, rank, lines0, indices, k, M) for t in triplets)
    #Pick out the good pairs
    lines2 = triplets[par_res]
    elapsed = timeit.default_timer() - start_time
    logger.info('Lines2 shape: {}.'.format(lines2.shape))
    logger.info("Lines2 made in {} seconds.".format(elapsed))

    np.save(fname+'_2', lines2)

    #Search for models
    #   Order lines2
    start_time = timeit.default_timer()
    lines2ordered = list()
    lines2ordered.append(list())
    for i in range(len(lines2)-1):
        lines2ordered[-1].append(lines2[i])
        if not lines2[i][1] == lines2[i+1][1]:
            lines2ordered.append(list())
    lines2ordered = [np.array(lines) for lines in lines2ordered[:-1]]
    elapsed = timeit.default_timer() - start_time
    logger.info("Lines2 ordered in {} seconds.".format(elapsed))

    #   Make quadruples
    n_proc = mp.cpu_count()
    chunksize = len(lines2ordered) // n_proc
    chunks_lines2ord = list()
    for i_proc in range(n_proc):
        chunkstart = i_proc * chunksize
        chunkend = (i_proc + 1) * chunksize if i_proc < n_proc - 1 else None
        chunks_lines2ord.append(lines2ordered[slice(chunkstart, chunkend)])

    start_time = timeit.default_timer()
    pool = mp.Pool(processes=n_proc)
    proc_results = [pool.map_async(make_quadruples,chunk) for chunk in chunks_lines2ord]
    result_chunks = [r.get() for r in proc_results]
    quadruples = np.concatenate(np.hstack((result_chunks)))
    pool.close()
    logger.info('Quadruples shape before: {}.'.format(quadruples.shape))
    quadruples = np.unique(np.sort(quadruples), axis=0)
    logger.info('Quadruples shape after: {}.'.format(quadruples.shape))
    elapsed = timeit.default_timer() - start_time
    logger.info("Quadruples made in {} seconds.".format(elapsed))
    np.save(fname+'_4a', quadruples)
    #   Make select good models
    start_time = timeit.default_timer()
    par_res = Parallel(n_jobs=n_proc, verbose=verbose)(delayed(check_model)(q, lines0, indices, rank, k, M) for q in quadruples)
    #Pick out the good pairs
    logger.info('par_res true: {}, all: {}'.format(np.sum(par_res), len(par_res)))
    if np.sum(par_res) == 0:
        logger.warning('No models found. Abandon.')
        return 0
    models = quadruples[par_res]
    elapsed = timeit.default_timer() - start_time
    logger.info("Models found in {} seconds.".format(elapsed))
    logger.info('Models shape: {}.'.format(models.shape))
    np.save(fname+'_4b', models)
    #   Save models
    missing_i = Parallel(n_jobs=n_proc, verbose=verbose)(delayed(complete_tuple)(m, lines0) for m in models)
    full_models = np.zeros((len(models), 5))
    full_models[:,0:4] += models
    full_models[:,4] += np.array(missing_i)
    print(full_models.shape)
    full_models = np.unique(np.sort(full_models), axis = 0).astype(np.int)
    logger.info('Full models shape before: {}.'.format(full_models.shape))
    np.save(fname+'_5', full_models)
    GUT = list()
    if len(models) != 0:
        five_models = np.array([lines0[m] for m in full_models]).astype(np.int)
        np.save(fname+'_models', five_models)
        logger.info('Checking for GUT models.')
        par_res = Parallel(n_jobs=n_proc, verbose=verbose)(delayed(check_model_representations)(m, M) for m in five_models)
        GUT = five_models[par_res]
    else:
        np.save(fname+'_models', list())

    np.save(fname+'_GUTmodels', GUT)
    logger.info(str(len(models))+" models found.")
    logger.info(str(len(GUT))+" GUT models found.")
