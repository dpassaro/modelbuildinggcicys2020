# Line Bundle Models on gCICYS

Repository for the code of our paper [Heterotic Line Bundle Models on gCICYs](https://arxiv.org/abs/2010.XXXXX).

Authors: Magdalena Larfors, Davide Passaro, Robin Schneider

## Scans

The *Scans* folder includes python scripts (*gCICY1.py* and *gCICY2.py*) for two gCICYs to compute various topological quantities. They are both based on the [pyCICY](https://github.com/robin-schneider/CICY) package. It further includes a script *scan.py* which is a python implementation of the first line bundle model [scans](https://arxiv.org/abs/1307.4787). There is no stability check for V included in the scan. Stability is checked numerically in *stability_check.py* using a python [interface](https://github.com/WolframResearch/WolframClientForPython) to mathematica.

## Smoothness

The *Smoothness* folder includes Jupyter and Mathematica notebooks which were used to study the smoothness of the gCICYs and their discrete symmetries. For each gCICY there is a folder containing two files, one named smoothness and one named Z2. In the smoothness file only the smoothness of the most general gCICY is stidied and in the Z2 file its transformations with regard to the Z2 symmetry is analyzed.

## Results

The results for a scan with qmax = 6 can be found in the two subdirectories *Scans/gCICY1* and *Scans/gCICY1*. The number of (putative) standard like models compared to two CICYs which are homotopically equivalent to X1 are shown in the picture below.

![Models found on X1 and X2 for a given qmax.](nmodels.png)


 

